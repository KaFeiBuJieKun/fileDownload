﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QFile>
#include <QTimer>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    qint64 getFileTotalSize(QString url);
    QString doubleToQString(const double &dbNum);
    QString formatSize(qint64 size);
    void showFileSize();

private slots:
    void on_downLoad_clicked();

    void on_choosePath_clicked();
    void on_finished();
    void on_readyRead();
    void on_downloadProgress(qint64 bytesRead, qint64 totalBytes);
    void on_urlLineEdit_textChanged(const QString &arg1);

    void showSpeed();

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;   //网络响应

    QString downLoadUrl;
    QFile *downloadedFile;
    qint64 hadLoadSize;   //已下载的文件大小
    QTimer *mtimer;
    QTime startTime;
    QTime stopTime;
};

#endif // MAINWINDOW_H
